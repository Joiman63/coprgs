#include <stdio.h>
#include <math.h>
int decimal(int n);
int binary(int n);
int main()
{
   int n;
   int ch;
   printf("1.Convert binary to decimal.\n");
   printf("2.Convert decimal to binary.\n");
   scanf("%d",&ch);
   if (ch==1)
   {
       printf("Enter a binary number: ");
       scanf("%d", &n);
       printf("%d in binary is %d in decimal", n, decimal(n));
   }
   if (ch==2)
   {
       printf("Enter a decimal number: ");
       scanf("%d", &n);
       printf("%d in decimal is %d in binary", n, binary(n));
   }
   return 0;
}

int binary(int n)
{
    int rem, i=1, bin=0;
    while (n!=0)
    {
        rem=n%2;
        n/=2;
        bin=bin + rem*i;
        i*=10;
    }
    return bin;
}

int decimal(int n)
{
    int deci=0, i=0, rem;
    while (n!=0)
    {
        rem = n%10;
        n/=10;
        deci =deci + rem*pow(2,i);
        ++i;
    }
    return deci;
}

