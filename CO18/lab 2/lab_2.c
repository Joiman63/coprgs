#include<stdio.h>

void convert_base(int,int);
int main()
{
  int num, choice, base;
    while(1)
    {
        printf("Conversion: \n\n");
        printf("1. Decimal to hexadecimal. \n");
        printf("2. Decimal to octal. \n");
        printf("3. Exit. \n");

        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch(choice)
        {
            case 1:base = 16;
                   break;
            case 2:base = 8;
                   break;
            case 3:printf("Exit mode");
                   exit(1);
            default:printf("Invalid choice.\n\n");
                   continue;
        }

        printf("Enter a number: ");
        scanf("%d", &num);

        printf("Result = ");
        convert_base(num, base);
        printf("\n\n");
    }

    return 0;
}


void convert_base(int num, int base)
{
    int rem;

    if (num == 0)
    {
        return;
    }
    else
    {
        rem = num % base;
        convert_base(num/base, base);
        if(base == 16 && rem >= 10)
        {
            printf("%c", rem+55);
        }
        else
        {
            printf("%d", rem);
        }
    }

}
















































